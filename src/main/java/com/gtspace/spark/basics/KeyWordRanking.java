package com.gtspace.spark.basics;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class KeyWordRanking {
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "C:/Users/gautiwar1/Documents/Learning/spark");
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> initialRdd = sc.textFile("src/main/resources/subtitles/input.txt");

        JavaRDD<String> lettersOnlyRdd = initialRdd.map(sentence -> sentence.replaceAll("[^a-zA-Z\\s]", "").toLowerCase());

        JavaRDD<String> removedBlankLines = lettersOnlyRdd.filter(sentence -> sentence.trim().length() > 0);

        JavaRDD<String> justWords = removedBlankLines.flatMap(sentence -> Arrays.asList(sentence.split(" ")).iterator());

        JavaRDD<String> blankWordsRemoved = justWords.filter(word -> word.trim().length() > 0);

        JavaRDD<String> justInterestingWords = blankWordsRemoved.filter(word -> Util.isNotBoring(word));

        JavaPairRDD<String, Long> pairRdd = justInterestingWords.mapToPair(word -> new Tuple2<String, Long>(word, 1L));

        JavaPairRDD<String, Long> totals = pairRdd.reduceByKey((value1, value2) -> value1 + value2);

        JavaPairRDD<Long, String> switched = totals.mapToPair(tuple -> new Tuple2<Long, String>(tuple._2, tuple._1));

        JavaPairRDD<Long, String> sorted = switched.sortByKey(false);

        List<Tuple2<Long, String>> results = sorted.take(10);
        results.forEach(System.out::println);

            // instead of take if we want get all records in sorted order then foreach doesnt give the correct result
            // Because foreach executed the lambda function in parallel on each partition.
           //sorted.foreach(System.out::println);

            // use of coalesce which put all reocrds in single partitions and then sort returns the sorted records
            //sorted.collect().forEach(System.out::println);
            // any other function other than foreachwill work
            //sorted = sorted.coalesce(1);
            //sorted.collect().forEach(System.out::println);

        //================ // we can view spark job ui at localhost:4040 
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        
        sc.close();
    }

}
