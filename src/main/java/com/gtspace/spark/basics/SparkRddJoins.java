package com.gtspace.spark.basics;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class SparkRddJoins {
    public static void main(String[] args) {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Tuple2<Integer, Integer>> visitsRaw = new ArrayList<>();

        visitsRaw.add(new Tuple2<>(4, 18));
        visitsRaw.add(new Tuple2<>(6, 4));
        visitsRaw.add(new Tuple2<>(10, 9));

        List<Tuple2<Integer, String>> usersRaw = new ArrayList<>();
        usersRaw.add(new Tuple2<>(1, "John"));
        usersRaw.add(new Tuple2<>(2, "Bob"));
        usersRaw.add(new Tuple2<>(3, "Alan"));
        usersRaw.add(new Tuple2<>(4, "Doris"));
        usersRaw.add(new Tuple2<>(5, "Maryho"));
        usersRaw.add(new Tuple2<>(6, "Raquel"));
        JavaPairRDD<Integer, Integer> visits = sc.parallelizePairs(visitsRaw);
        JavaPairRDD<Integer, String> users = sc.parallelizePairs(usersRaw);

        // Inner join
        // JavaPairRDD<Integer, Tuple2<Integer, String>> joinedRdd = visits.join(users);

        // Left Outer Join
        // JavaPairRDD<Integer, Tuple2<Integer, Optional<String>>> joinedRdd = visits.leftOuterJoin(users);
        //joinedRdd.collect().forEach(it-> System.out.println(it._2._2.orElse("blank").toUpperCase()));


        // Right Outer Join
        //JavaPairRDD<Integer, Tuple2<Optional<Integer>, String>> joinedRdd = visits.rightOuterJoin(users);
        //joinedRdd.collect().forEach(it-> System.out.println("User " +it._2._2+" had "+ it._2._1.orElse(0)));

        // Full Outre join
        // JavaPairRDD<Integer, Tuple2<Optional<Integer>, Optional<String>>> joinedRdd = visits.fullOuterJoin(users);

        // /Cartesian join / cross join
        JavaPairRDD<Tuple2<Integer, Integer>, Tuple2<Integer, String>> joinedRdd = visits.cartesian(users);

        joinedRdd.collect().forEach(System.out::println);
        sc.close();
    }
}
