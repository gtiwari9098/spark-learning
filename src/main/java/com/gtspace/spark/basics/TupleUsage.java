package com.gtspace.spark.basics;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;
import scala.Tuple5;

import java.util.ArrayList;
import java.util.List;

public class TupleUsage {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        List<Integer> inputData = new ArrayList<>();
        inputData.add(35);
        inputData.add(12);
        inputData.add(90);
        inputData.add(20);
        SparkConf conf = new SparkConf().setAppName("startingspark").setMaster("local[*]");

        JavaSparkContext sc = new JavaSparkContext(conf); // entry point to connect to spark clustor
        JavaRDD<Integer> originalRdd =  sc.parallelize(inputData);

        JavaRDD<Tuple2<Integer,Double>> sqrtRdd = originalRdd.map(value -> new Tuple2<>(value,Math.sqrt(value)));

        // Tuple provide convenient way keeping related value together
        new Tuple5<>(1,2,3,4,5);

        sc.close();
    }
}
