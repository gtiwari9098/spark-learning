package com.gtspace.spark.basics;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.List;

public class SparkMain {

    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        List<Double> inputData = new ArrayList<>();
        inputData.add(35.5);
        inputData.add(12.49943);
        inputData.add(90.32);
        inputData.add(20.32);
        SparkConf conf = new SparkConf().setAppName("startingspark").setMaster("local[*]");

        JavaSparkContext sc = new JavaSparkContext(conf); // entry point to connect to spark clustor

        JavaRDD<Double> myRdd = sc.parallelize(inputData); // it loads the data

        Double result= myRdd.reduce((value1,value2)-> value1+value2);
        System.out.println(result);
        sc.close();
    }
}
