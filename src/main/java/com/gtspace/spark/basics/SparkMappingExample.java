package com.gtspace.spark.basics;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.List;

public class SparkMappingExample {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        List<Integer> inputData = new ArrayList<>();
        inputData.add(35);
        inputData.add(12);
        inputData.add(90);
        inputData.add(20);
        SparkConf conf = new SparkConf().setAppName("startingspark").setMaster("local[*]");

        JavaSparkContext sc = new JavaSparkContext(conf); // entry point to connect to spark clustor

        JavaRDD<Integer> myRdd = sc.parallelize(inputData); // it loads the data

        Integer result = myRdd.reduce((value1, value2) -> value1 + value2);
        System.out.println(result);

        JavaRDD<Double> sqrtRdd = myRdd.map(value -> Math.sqrt(value));
        //sqrtRdd.foreach(System.out::println); // gettting serializable exception because println is not serailizable and RDD are ditributedovermultiple CPU
        sqrtRdd.collect().forEach(System.out::println);

        System.out.println(sqrtRdd.count());
        // or count just map and reduce instead of count
        Long  count = sqrtRdd.map(value ->1L).reduce((val1,val2) -> val1+val2);
        System.out.println(count);



        sc.close();
    }
}