package com.gtspace.spark.bigdataexercise;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple1;
import scala.Tuple2;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        ArrayList<Tuple2<Integer, Integer>> chapterTuples = new ArrayList<Tuple2<Integer, Integer>>();
        chapterTuples.add(new Tuple2<>(96,1));
        chapterTuples.add(new Tuple2<>(97,1));
        chapterTuples.add(new Tuple2<>(98,1));
        chapterTuples.add(new Tuple2<>(99,2));
        chapterTuples.add(new Tuple2<>(100,3));
        chapterTuples.add(new Tuple2<>(101,3));
        chapterTuples.add(new Tuple2<>(102,3));
        chapterTuples.add(new Tuple2<>(103,3));
        chapterTuples.add(new Tuple2<>(104,3));
        chapterTuples.add(new Tuple2<>(105,3));
        chapterTuples.add(new Tuple2<>(106,3));
        chapterTuples.add(new Tuple2<>(107,3));
        chapterTuples.add(new Tuple2<>(108,3));
        chapterTuples.add(new Tuple2<>(109,3));

        JavaPairRDD<Integer, Integer> chapterDataRDD = sc.parallelizePairs(chapterTuples);
        JavaPairRDD<Integer, Integer> chapterCountRDD = chapterDataRDD.mapToPair(row -> new Tuple2<Integer, Integer>(row._2, 1)).reduceByKey((value1, value2) -> value1 + value2);
        chapterCountRDD.collect().forEach(System.out::println);
    }
}
